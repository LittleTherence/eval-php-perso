<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require dirname(__DIR__) . '/lib/autoload.php';
require dirname(__DIR__) . '/lib/router.php';

