<?php

namespace App\Controller;

use App\Entity\Appointment;
use Plugo\Controller\AbstractController;
use Plugo\Services\Flash\Flash;

class AppController extends AbstractController {

  public function index() {
    // var_dump(Bar::helloWorld());
    // $appointmentManager = new AppointmentManager();

    return $this->renderView('appointment/index.php', []);

  }
}
