<?php

namespace Plugo\Services\Flash;

class Flash {

    public static function setFlash($message, $status) {
        $_SESSION['message'] = $message;
        $_SESSION['status'] = $status;
    }

    public static function getFlash() {
        if(isset($_SESSION['message']) && $_SESSION['message'] !== null && isset($_SESSION['status']) && $_SESSION['status'] !== null) {

            $data = [
                "message" => $_SESSION['message'],
                "status" => $_SESSION['status'],
            ];
            self::removeFlash();

            return $data;
        }

        return null;
    }

    public static function removeFlash() {
        unset($_SESSION['message']);
        unset($_SESSION['status']);
    }

    public static function displayFlash() {
        if($data = self::getFlash()) {
            return "<div class='container'><blockquote class='".$data['status']."'>".$data['message']."</blockquote></div>";
        }
    }
}