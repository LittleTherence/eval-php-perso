<?php

namespace Plugo\Services\Auth;

class Authentificator {

    public static function login() {
        return 'login';
    }


    public static function register() {
        return 'register';
    }

    public static function logout() {
        return 'logout';
    }   

    public static function isLoggedIn() {
        return 'isLoggedIn';
    }
}