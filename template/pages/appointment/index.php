<h1>Mon agenda</h1>

<h2>
        RDV depuis readManyBy():
</h2>
<?php foreach($data['test'] as $appointment) { ?>
  <article>
    <h2>
      <?php if($appointment->getImportant() === 1) { ?>
        <i class="fa-solid fa-circle-exclamation"></i>
      <?php } ?>
      <?= htmlspecialchars($appointment->getTitle()) ?>
    </h2>
    <p><?= $appointment->displayDate() ?> <?= $appointment->displayTime() ?></p>
    <a href="?page=show&id=<?= $appointment->getId() ?>" role="button">Détails</a>
  </article>
<?php } ?>
<hr>
<h2>
        RDV depuis readMany():
</h2>
<?php foreach($data['appointments'] as $appointment) { ?>
  <article>
    <h2>
      <?php if($appointment->getImportant() === 1) { ?>
        <i class="fa-solid fa-circle-exclamation"></i>
      <?php } ?>
      <?= htmlspecialchars($appointment->getTitle()) ?>
    </h2>
    <p><?= $appointment->displayDate() ?> <?= $appointment->displayTime() ?></p>
    <a href="?page=show&id=<?= $appointment->getId() ?>" role="button">Détails</a>
  </article>
<?php } ?>
<h2>
  RDV depuis FindOneBy():
</h2>
<?php foreach($data['testOne'] as $appointment) { ?>
  <article>
    <h2>
      <?php if($appointment->getImportant() === 1) { ?>
        <i class="fa-solid fa-circle-exclamation"></i>
      <?php } ?>
      <?= htmlspecialchars($appointment->getTitle()) ?>
    </h2>
    <p><?= $appointment->displayDate() ?> <?= $appointment->displayTime() ?></p>
    <a href="?page=show&id=<?= $appointment->getId() ?>" role="button">Détails</a>
  </article>
<?php } ?>